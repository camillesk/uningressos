require 'sidekiq/web'

Rails.application.routes.draw do
  resources :contacts
  resources :user_achievements
  resources :products
  resources :achievements
  resources :purchases
  resources :coupons
  resources :combos
  resources :waiting_lists
  resources :payment_methods
  resources :events
  devise_for :users, controllers: { sessions: "users/sessions", registrations: "users/registrations" }

  root :to => "events#index"

  # Sidekiq web config
  mount Sidekiq::Web => '/sidekiq'

  Sidekiq::Web.use(Rack::Auth::Basic) do |user, password|
    [user, password] == [ENV["SIDEKIQ_USERNAME"], ENV["SIDEKIQ_PASSWORD"]]
  end

  Sidekiq::Web.set :session_secret, Rails.application.secrets[:secret_key_base]
end
