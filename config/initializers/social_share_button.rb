SocialShareButton.configure do |config|
  config.allow_sites = %w(twitter facebook telegram whatsapp_web)
end
