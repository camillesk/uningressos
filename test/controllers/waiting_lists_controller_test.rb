require 'test_helper'

class WaitingListsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @waiting_list = waiting_lists(:one)
  end

  test "should get index" do
    get waiting_lists_url
    assert_response :success
  end

  test "should get new" do
    get new_waiting_list_url
    assert_response :success
  end

  test "should create waiting_list" do
    assert_difference('WaitingList.count') do
      post waiting_lists_url, params: { waiting_list: { entry_date: @waiting_list.entry_date, event_id: @waiting_list.event_id, user_id: @waiting_list.user_id } }
    end

    assert_redirected_to waiting_list_url(WaitingList.last)
  end

  test "should show waiting_list" do
    get waiting_list_url(@waiting_list)
    assert_response :success
  end

  test "should get edit" do
    get edit_waiting_list_url(@waiting_list)
    assert_response :success
  end

  test "should update waiting_list" do
    patch waiting_list_url(@waiting_list), params: { waiting_list: { entry_date: @waiting_list.entry_date, event_id: @waiting_list.event_id, user_id: @waiting_list.user_id } }
    assert_redirected_to waiting_list_url(@waiting_list)
  end

  test "should destroy waiting_list" do
    assert_difference('WaitingList.count', -1) do
      delete waiting_list_url(@waiting_list)
    end

    assert_redirected_to waiting_lists_url
  end
end
