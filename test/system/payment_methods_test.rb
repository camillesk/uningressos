require "application_system_test_case"

class PaymentMethodsTest < ApplicationSystemTestCase
  setup do
    @payment_method = payment_methods(:one)
  end

  test "visiting the index" do
    visit payment_methods_url
    assert_selector "h1", text: "Payment Methods"
  end

  test "creating a Payment method" do
    visit payment_methods_url
    click_on "New Payment Method"

    fill_in "Credit card", with: @payment_method.card_number
    fill_in "card_expiration", with: @payment_method.card_expiration
    fill_in "Security code", with: @payment_method.security_code
    fill_in "User", with: @payment_method.user_id
    click_on "Create Payment method"

    assert_text "Payment method was successfully created"
    click_on "Back"
  end

  test "updating a Payment method" do
    visit payment_methods_url
    click_on "Edit", match: :first

    fill_in "Credit card", with: @payment_method.card_number
    fill_in "card_expiration", with: @payment_method.card_expiration
    fill_in "Security code", with: @payment_method.security_code
    fill_in "User", with: @payment_method.user_id
    click_on "Update Payment method"

    assert_text "Payment method was successfully updated"
    click_on "Back"
  end

  test "destroying a Payment method" do
    visit payment_methods_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Payment method was successfully destroyed"
  end
end
