# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

## Ruby version

  * Ruby `2.5.5`

## System dependencies

  * `docker-compose` to use container or `redis`

## Configuration

  * run `docker-compose up`

  * run `rails s`

  * check `localhost:3000`

## Database creation

  * `rails db:create`

## Database initialization

  * `rails db:migrate`

## How to run the test suite

## Services (job queues, cache servers, search engines, etc.)
