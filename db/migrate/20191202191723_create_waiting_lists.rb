class CreateWaitingLists < ActiveRecord::Migration[5.2]
  def change
    create_table :waiting_lists do |t|
      t.integer :user_id
      t.integer :event_id
      t.datetime :entry_date

      t.timestamps
    end
  end
end
