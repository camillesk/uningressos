class CreateCombos < ActiveRecord::Migration[5.2]
  def change
    create_table :combos do |t|
      t.integer :event_id
      t.integer :product_id
      t.string :name
      t.float :price
      t.integer :qty

      t.timestamps
    end
  end
end
