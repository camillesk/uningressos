class CreateContacts < ActiveRecord::Migration[5.2]
  def change
    create_table :contacts do |t|
      t.integer :user
      t.text :message
      t.integer :receiver
      t.integer :event

      t.timestamps
    end
  end
end
