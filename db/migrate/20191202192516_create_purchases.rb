class CreatePurchases < ActiveRecord::Migration[5.2]
  def change
    create_table :purchases do |t|
      t.string :purchase_type
      t.integer :user_id
      t.datetime :date
      t.integer :coupon_id
      t.integer :bought_id
      t.float :value

      t.timestamps
    end
  end
end
