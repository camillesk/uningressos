class CreateCoupons < ActiveRecord::Migration[5.2]
  def change
    create_table :coupons do |t|
      t.integer :qty
      t.integer :event_id
      t.float :discount
      t.string :code

      t.timestamps
    end
  end
end
