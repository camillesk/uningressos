class CreatePaymentMethods < ActiveRecord::Migration[5.2]
  def change
    create_table :payment_methods do |t|
      t.integer :user_id
      t.string :nickname
      t.string :card_holder
      t.bigint :card_number
      t.integer :card_expiration_month
      t.integer :card_expiration_year

      t.timestamps
    end
  end
end
