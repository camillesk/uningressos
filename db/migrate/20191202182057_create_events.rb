class CreateEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :events do |t|
      t.string :name
      t.text :description
      t.datetime :date
      t.integer :qty
      t.float :price
      t.string :category
      t.string :owner

      t.timestamps
    end
  end
end
