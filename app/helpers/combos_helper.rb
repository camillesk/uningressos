module CombosHelper

  def my_products
    array = []
    events = Event.where(owner: current_user.name).to_a

    products = Product.where(event_id: events)

    products.each do |product|
      array << [product.name, product.id]
    end

    array
  end
end
