module ApplicationHelper

  def my_events
    array = []
    events = Event.where(owner: current_user.name)

    events.each do |event|
      array << [event.name, event.id]
    end

    array
  end
end
