module PurchasesHelper

  def payment_methods
    PaymentMethod.where(user_id: current_user.id).pluck(:nickname)
  end
end
