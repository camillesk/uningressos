module EventsHelper

  def events_categories
    [
      ['Games', 'games'],
      ['Festa', 'festa'],
      ['Programação', 'progamacao'],
      ['Música', 'musica'],
      ['Cinema', 'cinema'],
      ['Palestra', 'palestra'],
      ['Outros', 'outros']
    ]
  end
end
