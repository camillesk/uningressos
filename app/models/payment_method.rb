class PaymentMethod < ApplicationRecord
  validates_presence_of :payment_method
end
