class EventsController < ApplicationController
  before_action :set_event, only: [:show, :edit, :update, :destroy]

  # GET /events
  # GET /events.json
  def index
    if current_user.admin?
      if params[:name] || params[:description] || params[:owner]
        @events = Event.where(
          'name LIKE :name AND description LIKE :description AND owner LIKE :owner AND category LIKE :category',
          name: "%#{params[:name]}%",
          description: "%#{params[:description]}%",
          owner: "%#{current_user.name}%",
          category: "%#{params[:category]}%"
        )
      else
        @events = Event.where(owner: current_user.name)
      end
    else
      if params[:name] || params[:description] || params[:owner]
        @events = Event.where(
          'name LIKE :name AND description LIKE :description AND owner LIKE :owner AND category LIKE :category',
          name: "%#{params[:name]}%",
          description: "%#{params[:description]}%",
          owner: "%#{params[:owner]}%",
          category: "%#{params[:category]}%"
        )
      else
        @events = Event.all
      end
    end
  end

  # GET /events/1
  # GET /events/1.json
  def show
    @owner = User.where(name: @event.owner).first
    @products = Product.where(event_id: @event.id).to_a
  end

  # GET /events/new
  def new
    @event = Event.new
  end

  # GET /events/1/edit
  def edit
  end

  # POST /events
  # POST /events.json
  def create
    params[:event][:owner] = current_user.name
    @event = Event.new(event_params)

    respond_to do |format|
      if @event.save
        format.html { redirect_to @event, notice: 'Event was successfully created.' }
        format.json { render :show, status: :created, location: @event }
      else
        format.html { render :new }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /events/1
  # PATCH/PUT /events/1.json
  def update
    respond_to do |format|
      if @event.update(event_params)
        format.html { redirect_to @event, notice: 'Event was successfully updated.' }
        format.json { render :show, status: :ok, location: @event }
      else
        format.html { render :edit }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /events/1
  # DELETE /events/1.json
  def destroy
    @event.destroy
    respond_to do |format|
      format.html { redirect_to events_url, notice: 'Event was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
  def set_event
    @event = Event.find(params[:id])
  end

  # Never truANDANDst parameters from the scary internet, only allow the white list through.
  def event_params
    params.require(:event).permit(:name, :qty, :description, :date, :price, :owner, :category)
  end
end
