class ContactsController < ApplicationController
  before_action :set_contact, only: [:show, :edit, :update, :destroy]

  # GET /contacts
  # GET /contacts.json
  def index
    @contacts = Contact.where(user: current_user.id)
  end

  # GET /contacts/1
  # GET /contacts/1.json
  def show
  end

  # GET /contacts/new
  def new
    @contact = Contact.new
  end

  # GET /contacts/1/edit
  def edit
  end

  # POST /contacts
  # POST /contacts.json
  def create
    byebug
    @contact = Contact.new(contact_params)
    if params[:contact][:broadcast] == "true"
      user_ids = Purchase.where(bought_id: params[:contact][:event]).pluck(:user_id).uniq!
      user_ids.each do |ui|
        user = User.find(ui)
        ContactMailer.with(user_name: current_user.name, user_email: current_user.email, event: @event_name, receiver_name: user.name, receiver_email: user.email, message: params[:contact][:message]).contact_mail.deliver_now
      end
    else
      @receiver_name = User.find(params[:contact][:receiver]).name
      @receiver_email = User.find(params[:contact][:receiver]).email
      @event_name = Event.find(params[:contact][:event]).name

      ContactMailer.with(user_name: current_user.name, user_email: current_user.email, event: @event_name, receiver_name: @receiver_name, receiver_email: @receiver_email, message: params[:contact][:message]).contact_mail.deliver_now
    end

    respond_to do |format|
      if @contact.save
        format.html { redirect_to @contact, notice: 'E-mail enfileirado para envio' }
        format.json { render :show, status: :created, location: @contact }
      else
        format.html { render :new }
        format.json { render json: @contact.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /contacts/1
  # PATCH/PUT /contacts/1.json
  def update
    respond_to do |format|
      format.html { render :new }
    end
  end

  # DELETE /contacts/1
  # DELETE /contacts/1.json
  def destroy
    @contact.destroy
    respond_to do |format|
      format.html { redirect_to contacts_url, notice: 'Contact was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_contact
      @contact = Contact.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def contact_params
      params.require(:contact).permit(:user, :message, :receiver, :event)
    end
end
