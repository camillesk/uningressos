class PurchasesController < ApplicationController
  before_action :set_purchase, only: [:show, :edit, :update, :destroy]

  # GET /purchases
  # GET /purchases.json
  def index
    @purchases = Purchase.where(user_id: current_user.id).to_a
  end

  # GET /purchases/1
  # GET /purchases/1.json
  def show
    klass = @purchase.purchase_type.classify.constantize
    @item = klass.find(@purchase.bought_id).name
  end

  # GET /purchases/new
  def new
    @purchase = Purchase.new
  end

  # GET /purchases/1/edit
  def edit
  end

  # POST /purchases
  # POST /purchases.json
  def create

    params[:purchase][:date] = DateTime.now
    klass = params[:purchase][:purchase_type].classify.constantize
    value = klass.find(params[:purchase][:bought_id]).price

    if klass.find(params[:purchase][:bought_id]).qty <= 0
      flash[:notice] = "Não ha vagas para o evento"
      redirect_to purchases_path
      return
    end

    if !params[:purchase][:coupon].blank? && Coupon.where(code: params[:purchase][:coupon]).qty > 0
      coupon_id = Coupon.where(code: params[:purchase][:coupon]).first.id
      discount = Coupon.find(coupon_id).discount
      params[:purchase][:value] = value - discount
      params[:purchase][:coupon_id] = coupon_id
      coupon = Coupon.where(code: params[:purchase][:coupon])
      coupon.qty -= 1
      coupon.save
    else
      params[:purchase][:value] = value
    end

    item = klass.find(params[:purchase][:bought_id])
    item.qty -= 1
    item.save

    @purchase = Purchase.new(purchase_params)

    respond_to do |format|
      if @purchase.save
        format.html { redirect_to @purchase, notice: 'Purchase was successfully created.' }
        format.json { render :show, status: :created, location: @purchase }
      else
        format.html { render :new }
        format.json { render json: @purchase.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /purchases/1
  # PATCH/PUT /purchases/1.json
  def update
    respond_to do |format|
      if @purchase.update(purchase_params)
        format.html { redirect_to @purchase, notice: 'Purchase was successfully updated.' }
        format.json { render :show, status: :ok, location: @purchase }
      else
        format.html { render :edit }
        format.json { render json: @purchase.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /purchases/1
  # DELETE /purchases/1.json
  def destroy
    @purchase.destroy
    respond_to do |format|
      format.html { redirect_to purchases_url, notice: 'Purchase was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_purchase
      @purchase = Purchase.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def purchase_params
      params.require(:purchase).permit(:purchase_type, :user_id, :date, :coupon_id, :bought_id, :value)
    end
end
