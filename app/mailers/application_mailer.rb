class ApplicationMailer < ActionMailer::Base
  default from: 'camille.bettini@gmail.com'
  layout 'mailer'
end
