class ContactMailer < ApplicationMailer
  def contact_mail
    @user_name = params[:user_name]
    @user_email = params[:user_email]
    @event = params[:event]
    @receiver_name = params[:receiver_name]
    @receiver_email = params[:receiver_email]
    @message = params[:message]

    mail(to: @receiver_email, subject: "Contato sobre o evento #{@event}")
  end
end
