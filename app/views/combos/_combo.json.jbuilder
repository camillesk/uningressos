json.extract! combo, :id, :event_id, :product_id, :name, :price, :created_at, :updated_at
json.url combo_url(combo, format: :json)
