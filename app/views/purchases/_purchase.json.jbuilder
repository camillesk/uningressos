json.extract! purchase, :id, :purchase_type, :user_id, :date, :coupon_id, :bought_id, :created_at, :updated_at
json.url purchase_url(purchase, format: :json)
