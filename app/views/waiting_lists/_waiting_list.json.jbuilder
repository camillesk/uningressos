json.extract! waiting_list, :id, :user_id, :event_id, :entry_date, :created_at, :updated_at
json.url waiting_list_url(waiting_list, format: :json)
