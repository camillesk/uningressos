json.extract! coupon, :id, :qty, :event_id, :discount, :created_at, :updated_at
json.url coupon_url(coupon, format: :json)
