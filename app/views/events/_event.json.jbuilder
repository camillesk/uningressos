json.extract! event, :id, :name, :vacancy, :description, :date, :price, :created_at, :updated_at
json.url event_url(event, format: :json)
