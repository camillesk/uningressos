json.extract! contact, :id, :user, :message, :receiver, :event, :created_at, :updated_at
json.url contact_url(contact, format: :json)
