json.extract! payment_method, :id, :user_id, :card_number, :security_code, :card_expiration_month, :card_expiration_year, :created_at, :updated_at
json.url payment_method_url(payment_method, format: :json)
