json.extract! user_achievement, :id, :id_achievement, :user_id, :created_at, :updated_at
json.url user_achievement_url(user_achievement, format: :json)
